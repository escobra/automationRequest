var config = require('./config'),
	mongoose = require('mongoose');

module.exports = function() {
	var db = mongoose.connect(config.db);
	require('../app/models/approver.server.model');
	require('../app/models/qualifier.server.model');
	require('../app/models/usergroup.server.model');
	require('../app/models/module.server.model');
	require('../app/models/application.server.model');
	require('../app/models/request.server.model');
	require('../app/models/qualifierValue.server.model');
	
	return db;
};