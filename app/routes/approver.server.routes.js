module.exports = function(app) {
	var approver = require('../controllers/approver.server.controller');
	app.route('/approver/findByDomainId').post(approver.findByDomainId);
	app.route('/approver').post(approver.create).get(approver.render);
	app.route('/approver/list').get(approver.list);
	app.route('/approver/list').post(approver.getSelected);
	app.route('/approver/:approverId').get(approver.read).delete(approver.delete);
	app.param('approverId', approver.getById);
};