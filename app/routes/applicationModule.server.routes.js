module.exports = function(app) {
	var application = require('../controllers/application.server.controller');
	app.route('/application').post(application.create);
	app.route('/application/list').get(application.list);
	app.route('/application/:applicationId').get(application.read).put(application.update).delete(application.delete);

	var module = require('../controllers/module.server.controller');
	app.route('/module').post(module.create)
	app.route('/module/list').get(module.list);
	app.route('/module/list').post(module.getSelected);
	app.route('/module/:moduleId').get(module.read).put(module.update).delete(module.delete);
	
	var applicationModule = require('../controllers/applicationModule.server.controller');
	app.route('/application/:applicationId/module/:moduleId').delete(applicationModule.delete);
	app.route('/applicationModule').get(applicationModule.render);
	app.route('/applicationModule/list').get(applicationModule.list);
	app.route('/save/:applicationId/:moduleId/:usergroupId/:qualifierId/:approverId').post(applicationModule.save);


	var functionUsergroup = require('../controllers/usergroup.server.controller');
	app.route('/FunctionUsergroup').post(functionUsergroup.create);
	app.route('/FunctionUsergroup/list').get(functionUsergroup.list);
	app.route('/usergroup/list').post(functionUsergroup.getSelected);

	var qualifierUsertype = require('../controllers/qualifier.server.controller');
	app.route('/qualifier/list').post(qualifierUsertype.getSelected);
	app.route('/QualifierUsertype').post(qualifierUsertype.create);
	app.route('/QualifierUsertype/list').get(qualifierUsertype.list);

	var qualifierUsertypeValue = require('../controllers/qualifierUsertypeValue.server.controller');
	app.route('/QualifierValue/list').post(qualifierUsertypeValue.getSelected);
	app.route('/QualifierValue').post(qualifierUsertypeValue.create);
	app.route('/QualifierValue/list').get(qualifierUsertypeValue.list);


	var approver = require('../controllers/approver.server.controller');

	app.param('applicationId', application.getById);
	app.param('moduleId', module.getById);
	app.param('usergroupId', functionUsergroup.getById);
	app.param('qualifierId', qualifierUsertype.getById);
	app.param('qualifierId', approver.getById);
};