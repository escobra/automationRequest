module.exports = function(app) {
	var request = require('../controllers/request.server.controller');
	app.route('/request/create').get(request.createView).post(request.create);
	app.route('/request/dashboard').get(request.createDashboardView).post(request.create);
	app.route('/request').get(request.render);
	app.route('/request/list').get(request.list);
	app.route('/request/searchBy').post(request.searchBy);
	app.route('/request/batchSend').post(request.batchSend);
	app.route('/request/approvrView').get(request.approvrView);
	app.route('/request/approverView').post(request.getRequestBySrNo);
	app.route('/request/updateStatus/:requestId').post(request.updateSrStatus);
	app.route('/request/:requestId').get(request.read).put(request.update).delete(request.delete);
	app.param('requestId', request.getById);
};