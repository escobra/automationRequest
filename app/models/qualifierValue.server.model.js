var mongoose = require('mongoose'),
Schema = mongoose.Schema;


var QualifiervalueSchema = new Schema({
	name:  { type: String, required: true, unique: true }
	,qualifiers : [{ type : Schema.Types.ObjectId, ref : 'Qualifier'}]
	,created_at: Date
	,updated_at: Date
});


QualifiervalueSchema.pre('save', function(next) {
	var currentDate = new Date();
	this.updated_at = currentDate;
	if (!this.created_at)
		this.created_at = currentDate;
	next();
});

mongoose.model('Qualifiervalue', QualifiervalueSchema);