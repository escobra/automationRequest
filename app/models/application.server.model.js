var mongoose = require('mongoose'),
Schema = mongoose.Schema;


var ApplicationSchema = new Schema({
	name:  { type: String, required: true, unique: true}
	,modules : [{ type: Schema.Types.ObjectId, ref: 'Module'}]
	,created_at: Date
	,updated_at: Date
});

ApplicationSchema.pre('save', function(next) {
	var currentDate = new Date();
	this.updated_at = currentDate;
	if (!this.created_at)
		this.created_at = currentDate;
	next();
});

mongoose.model('Application', ApplicationSchema);