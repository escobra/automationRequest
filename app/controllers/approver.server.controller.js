var Approver = require('mongoose').model('Approver'),
	fs = require("fs");

var getErrorMessage = function(err) {
	var message = '';
	for (var errName in err.errors) {
		if (err.errors[errName].message)
			message = err.errors[errName].message;
	}
	return message;
};


exports.render = function(req, res) {
	res.render('approver/index', {
		title: 'Maintain approver Module'
	});
};


exports.list = function(req, res, next) {
	Approver.find({}, function(err, approver) {
		if (err) {
			return next(err);
		}
		else {
			res.json(approver);
		}
	}).sort('name');
};

exports.create = function(req, res, next) {
	var approver = new Approver(req.body);
	approver.save(function(err) {
		if (err) {
			return next(err);
		}
		else {
			res.json(approver);
		}
	});
};

exports.read = function(req, res) {
	res.json(req.approver);
};

exports.getById = function(req, res, next, id) {
	Approver.findOne({
		_id: id
	},
	function(err, approver) {
		if (err) {
			if(id == 0){
				next();
			}else{
				return next(err);
			}
		}
		else {
			req.approver = approver;
			next();
		}
	}
	);
};

exports.delete = function(req, res, next) {
	req.approver.remove(function(err) {
		if (err) {
			return next(err);
		}
		else {
			res.json(req.approver);
		}
	})
};

exports.findByDomainId = function(req, res){
	Approver.findOne({ domainId : req.body.domainId}, function(err, approver){
		if(!err){
			res.json(approver);
		}else{
			res.send(null);
		}
	});
};

exports.getSelected = function(req, res, next){
	var approverIds = req.body.approverIds;
	if(approverIds){
		Approver.find({
		    '_id': { $in: 
		        approverIds.split(',')
		    }
		}, function(err, approvers){
		     if(err){
		     	return next(err);
		     }else{
		     	res.json(approvers);
		     }
		});	
	}else{
		res.send('No selected usergroup');
	}
};

exports.getRequestAccess = function(req, res, next){

};

