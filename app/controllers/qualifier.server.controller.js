var Qualifier = require('mongoose').model('Qualifier');

var getErrorMessage = function(err) {
	var message = '';
	for (var errName in err.errors) {
		if (err.errors[errName].message)
			message = err.errors[errName].message;
	}
	return message;
};

exports.create = function(req, res, next) {
	var qualifier = new Qualifier(req.body);
	qualifier.save(function(err) {
		if (err) {
			return next(err);
		}
		else {
			res.json(qualifier);
		}
	});
};


exports.list = function(req, res, next) {
	Qualifier.find({}, function(err, qualifiers) {
		if (err) {
			return next(err);
		}
		else {
			res.json(qualifiers);
		}
	});
};


exports.getById = function(req, res, next, id) {
	Qualifier.findOne({
		_id: id
	},
	function(err, qualifier) {
		if (err) {
			if(id == 0){
				next();
			}else{
				return next(err);
			}
		}
		else {
			req.qualifier = qualifier;
			next();
		}
	}
	).populate('approvers');
};


exports.getSelected = function(req, res, next){
	var qualifierIds = req.body.qualifierIds;
	if(qualifierIds){
		Qualifier.find({
		    '_id': { $in: 
		        qualifierIds.split(',')
		    }
		}, function(err, qualifiers){
		     if(err){
		     	return next(err);
		     }else{
		     	res.json(qualifiers);
		     }
		});	
	}else{
		res.send('No selected usergroup');
	}
};
