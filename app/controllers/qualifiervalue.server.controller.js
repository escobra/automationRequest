var Qualifier = require('mongoose').model('Qualifier');
var Qualifiervalue = require('mongoose').model('Qualifiervalue');

var getErrorMessage = function(err) {
	var message = '';
	for (var errName in err.errors) {
		if (err.errors[errName].message)
			message = err.errors[errName].message;
	}
	return message;
};

exports.create = function(req, res, next) {
	var qualifierValue = new Qualifiervalue(req.body);
	qualifierValue.save(function(err) {
		if (err) {
			console.log('Hie Error Create' + err);
			return next(err);
		}
		else {
			console.log('Hie no Error Create' + qualifierValue);
			res.json(qualifierValue);
		}
	});
};


exports.list = function(req, res, next) {
	Qualifiervalue.find({}, function(err, qualifiers) {
		if (err) {
			return next(err);
		}
		else {
			res.json(qualifiers);
		}
	});
};


exports.getById = function(req, res, next, id) {
	Qualifiervalue.findOne({
		_id: id
	},
	function(err, qualifierValue) {
		if (err) {
			if(id == 0){
				next();
			}else{
				return next(err);
			}
		}
		else {
			req.qualifierValue = qualifierValue;
			next();
		}
	}
	).populate('approvers');
};


exports.getSelected = function(req, res, next){
	var qualifierIds = req.body.qualifierIds;
	if(qualifierIds){
		Qualifiervalue.find({
		    '_id': { $in: 
		        qualifierIds.split(',')
		    }
		}, function(err, qualifiers){
		     if(err){
		     	return next(err);
		     }else{
		     	res.json(qualifiers);
		     }
		});	
	}else{
		res.send('No selected usergroup');
	}
};
